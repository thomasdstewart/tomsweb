---
# Display name
name: Thomas Stewart

# Is this the primary user of the site?
superuser: true

# Short bio (displayed in user profile at end of posts)
bio: Computer enthusiast and sysadmin

# Interests to show in About widget
interests:
- Computers
- Linux
- Debian

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:thomas@stewarts.org.uk'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/thomasstewart
- icon: github
  icon_pack: fab
  link: https://github.com/thomasdstewart
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/thomasdstewart
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "thomas@stewarts.org.uk"
---
