---
title: "Links"
date: 2020-04-05
---
[![FSF member number 1207](fsf.png)](http://www.fsf.org/register_form?referrer=1207)
[![FSF member since May 2003](417974.png)](https://my.fsf.org/join?referrer=417974)
[![Open Rights Group. I founder it. Founder 466](http://widgets.openrightsgroup.org/founding1000/i-founded/344713.png)](http://www.openrightsgroup.org/)
[![WTDA](https://wtda.org/mug.png)](https://wtda.org/)
[![IPv6](http://ipv6-test.com/button-ipv6-small.png)](http://ipv6-test.com/validate.php?url=www.stewarts.org.uk)
* https://gravatar.com/thomasdstewart
* https://about.me/thomas_d_stewart
* https://robots.org.uk/
