---
title: "Bash"
summary: ""
authors: ["thomas"]
tags: ["linux", "bash"]
categories: []
date: 2009-05-21
---
[Bash](https://www.gnu.org/software/bash/) is a good shell, my [.bashrc](https://raw.githubusercontent.com/thomasdstewart/dotfiles/master/.bashrc) is available for all to see!
