---
title: "Blank Operator Logo"
summary: ""
authors: ["thomas"]
tags: ["mobile", "nokia"]
categories: []
date: 2005-06-06
---
Place these files in a web dir and visit from mobile:
* [blankoperatorlogo.html](blankoperatorlogo.html)
* [blankoperatorlogo.nol](blankoperatorlogo.nol)

Also set mime time with, eg .htaccess: AddType image/vnd.nok-oplogo-color nol
