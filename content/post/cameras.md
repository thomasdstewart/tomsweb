---
title: "Cameras"
summary: ""
authors: ["thomas"]
tags: ["camera", "photography"]
categories: []
date: 2022-04-05 19:03:00
---
## Canon EOS 700D (2015)
https://www.canon.co.uk/for_home/product_finder/cameras/digital_slr/eos_700d/

### Lens
* [Canon EF-S 18-55mm f/3.5-5.6 IS II ](https://www.canon.co.uk/lenses/ef-s-18-55mm-f-3-5-5-6-is-ii-lens/) (2015, 11-34mm equiv full frame)
* [Canon EF-S 18-200mm f/3.5-5.6 IS](https://www.canon.co.uk/lenses/ef-s-18-200mm-f-3-5-5-6-is-lens/) (2017, 11-125mm equiv full frame)
* [Canon EF-S 24mm f/2.8 STM](https://www.canon.co.uk/lenses/ef-s-24mm-f-2-8-stm-lens/) (2019?, 15mm equiv full frame)
* [Canon EF-S 10-18mm f/4.5-5.6 IS STM](https://www.canon.co.uk/lenses/ef-s-10-18mm-f-4-5-5-6-is-stm-lens/) (2022, 6-11mm equiv full frame)

http://www.bobatkins.com/photography/tutorials/crop_sensor_cameras_and_lenses.html

## Canon IXUS 130 (2010)
## Canon Powershot A720 IS (2007)
