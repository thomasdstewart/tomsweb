---
title: "Cineworld have an public xml data source"
summary: ""
authors: ["thomas"]
tags: ["blog", "movies", "cineworld"]
categories: []
date: 2009-06-24 21:30:00
---
I just found out that Cineworld have a public xml data source. It's over in the [syndication subdir](http://www.cineworld.co.uk/syndication/) of their main site. There are more technical details over on my CineworldScrape page. I feel a bit sad in a way, my scraper has been working for more or less 3 years now in some form or another. The annoying thing is that they don't actually export all the data I use. Being quite visual I have grown used to using the thumbnails that each movie has to help me pick which films to see. Apart from this it's a great step forward. However the problem is there's know knowing if Cineworld will pull this service or even if they want it public. For now I'll continue to use my scraper.
