---
title: "Dropped Traffic"
summary: ""
authors: ["thomas"]
tags: ["linux", "networking", "iptables"]
categories: []
date: 2008-12-05
---
Looking at a years worth of dropped packets. Plotting the time of day on the X-axis and TCP/UDP port number on the Y-axis.

![drops.png](drops.png)
[drops.ps](drops.ps)
