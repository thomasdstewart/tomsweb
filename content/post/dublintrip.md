---
title: "Dublin Trip"
summary: ""
authors: ["thomas"]
tags: ["blog", "holiday", "ireland", "dublin"]
categories: []
date: 2009-11-20 21:58:00
---
Last week I got back from a little holiday in Dublin. I went over for my cousins graduation. I had an amassing time, it was a very relaxed and slow paced get away from everything. We went to all sorts of different cafes, restaurants and live music bars in the evening. It felt so luxurious to stay in a hotel in the centre of town, we were able to roll out of which ever bar we were in, hail a cab and be back home in no time. Coming back to a made bed and fresh towels can't be beaten.

A few places stood out. One night we went for a Mexican in [Acapulca](http://www.acapulco.ie/) which was amazing in it's self but it was topped off by their "Deep Fried Ice Cream" desert. We also went to a restaurant type cafe one day. I can't remember the name of it now, but it served the best hot chocolate I have ever had. Then there was the [Queen of Tarts](http://www.queenoftarts.ie/) which is a lovely little cafe with amazing carrot cake.

We did not stay in the hotel for the whole time, the last night one of my cousins friends put us up in their amazing house. They took us to a very strange but also very cool place that only served tea. It was called The [Tea Garden](http://www.tea-garden.eu/). It had one of the most relaxing atmospheres I have been in. It was divided up into small booths with raised floors, cushions and small tables. We took our shoes off and sat round the small table and just relaxed! The waitress then came round and was able to recommend which teas to try. It was cool socialising without booze. When the tea came the pots had little night lights to keep them warm. We then just whiled the time away chatting.

It was not all food and drink mind, we did fit in a trip to the book of kells. We finished the trip off on the last day with an late afternoon visit to Bewley's.
