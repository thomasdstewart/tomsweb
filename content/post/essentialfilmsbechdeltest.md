---
title: "Essential Films Bechdel Test"
summary: ""
authors: ["thomas"]
tags: ["movies"]
categories: []
date: 2013-01-31
aliases: [/tomsweb/EssentialFilmsBechdelTest/]
---
I recently found out about the [Bechdel Test](http://en.wikipedia.org/wiki/Bechdel_Test), I was interested in how many of my [Essential Films]({{< relref "essentialfilms.md" >}}) passed the test. So I used the [Bechdel Test Movie List](http://bechdeltest.com/) to cross referance. I did some horable html dumps and scripting to do this, after I finished I found out that they provide an [API](http://bechdeltest.com/api/v1/doc) that can search based on IMDB id . I currently have 79 films in my list of these 64 has rating listed, or 81% of them.

|Breakdown of results||
|--- | --- |
|Fewer than two women in these movies|23%|
|There are two or more women in this movie, but they don't talk to each other|27%|
|There are two or more women in this movie, but they only talk to each other about a man|9%|
|There are two or more women in this movie and they talk to each other|41%|

And here are the films that are rated:

|Fewer than two women in these movies|
|--- |
|2001: A Space Odyssey|
|Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb|
|For a Few Dollars More|
|Full Metal Jacket|
|Good Will Hunting|
|Indiana Jones and the Last Crusade|
|Indiana Jones and the Temple of Doom|
|Lawrence of Arabia|
|Papillon|
|Raiders of the Lost Ark|
|Saving Private Ryan|
|Snatch.|
|The Good, the Bad and the Ugly|
|The Great Escape|
|The Shawshank Redemption|

|There are two or more women in these movies, but they don't talk to each other|
|--- |
|Airplane!|
|Back to the Future|
|Blade|
|Blade Runner|
|Blazing Saddles|
|Catch Me If You Can|
|Clerks.|
|Forrest Gump|
|Goldfinger|
|Jaws|
|North By Northwest|
|Once Upon a Time in the West|
|Planes, Trains & Automobiles|
|The Big Lebowski|
|The Blues Brothers|
|The Departed|
|The Godfather|


|There are two or more women in these movies, but they only talk to each other about a man|
|--- |
|Ferris Bueller's Day Off|
|Paint Your Wagon|
|The Fugitive|
|The Graduate|
|The Shining|
|Trainspotting|


|There are two or more women in these movies and they talk to each other about something other than a man|
|--- |
|Aliens|
|Apollo 13|
|El laberinto del fauno (Pan's Labyrinth)|
|Erin Brockovich|
|Garden State|
|Gone with the Wind|
|Inception|
|Jurassic Park|
|Just Like Heaven|
|Legally Blonde|
|Little Miss Sunshine|
|Notting Hill|
|Play Misty for Me|
|Pulp Fiction|
|Requiem for a Dream|
|Schindler's List|
|Spirited Away|
|Taken|
|The Breakfast Club|
|The Matrix|
|The Silence of the Lambs|
|The Wicker Man|
|Three Colors: Blue|
|Uncle Buck|
|Walk the Line|
|Where Eagles Dare|

