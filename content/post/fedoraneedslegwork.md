---
title: "Fedora needs legwork"
summary: ""
authors: ["thomas"]
tags: ["blog", "linux", "fedora", "yum"]
categories: []
date: 2009-06-06 00:06:00
---
I played with fedora more today. There are loads of packages in the default repos these days. However I still find my self missing things from Debian. For example, I can't do a "yum search" while doing a "yum upgrade". Why? I also miss the small helper scripts that save on legwork. For example update-grub, in debian when a new kernel gets installed the initrd and grub gets updated and just works. With Fedora I have to manually update the grub config and if say I forget to create the initrd it, it means a trip to the 13 PC's I just upgraded!
