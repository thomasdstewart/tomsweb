---
title: "Firefox"
summary: ""
authors: ["thomas"]
tags: ["firefox"]
categories: []
date: 2009-05-21
aliases: [/tomsweb/Firefox/]
---
[Firefox](http://www.mozilla.com/firefox/) is an amazing browser, sometimes I think I would not browse as much as I do if it were not around. Here is my [user.js](user.js) which has taken many years to perfect.

I maintain a small list of essential add-ons for it:

* [Flashblock](https://addons.mozilla.org/firefox/433/)
* [UnMHT](https://addons.mozilla.org/firefox/addon/8051)
* [FireGestures](https://addons.mozilla.org/firefox/addon/6366)
* [AutoAuth](https://addons.mozilla.org/firefox/addon/4949)
* [Adblock Plus](https://addons.mozilla.org/firefox/addon/1865)
* [Firebug](https://addons.mozilla.org/firefox/addon/1843)
* [Greasemonkey](https://addons.mozilla.org/firefox/addon/748)
* [It's All Text!](https://addons.mozilla.org/firefox/addon/4125)
* [JavaScript Debugger](https://addons.mozilla.org/firefox/addon/216n)

