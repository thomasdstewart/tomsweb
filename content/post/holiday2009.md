---
title: "I'm off on my holidays"
summary: ""
authors: ["thomas"]
tags: ["blog", "holiday", "rss"]
categories: []
date: 2009-07-02 23:53:00
---
No Internet for 2 weeks, just as I catch up on my feeds too!

```
$ psql -c "select count(*) from rssr.entries where read = false;" rssr
 count 
 -------
      0
      (1 row)
$
```
