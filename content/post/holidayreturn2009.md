---
title: "I'm back from Ireland"
summary: ""
authors: ["thomas"]
tags: ["blog", "holiday", "rss"]
categories: []
date: 2009-07-24 00:18:00
---
I got back on Monday night after cutting it worryingly close to get to the airport, 2 hours in advance try 40 min! Only for Ryanair to delay the flight by an hour and tell no one. Then there's the Post Holiday Blues, aka [PTD](http://en.wikipedia.org/wiki/Post_vacation_blues). It hit bad on Tuesday night but I think I'm used to the grind stone again now.

Also I'm not laughing now:

```
$ psql -c "select count(*) from rssr.entries where read = false;" rssr
 count 
 -------
   9941
   (1 row)

$
```

In other news I might be going to see "STOCKWELL: THE INQUEST" in the Landor Theatre on Monday night. My post holiday resolution is to go to London for more culture. 
