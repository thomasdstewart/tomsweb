---
title: "Hugo"
summary: ""
authors: ["thomas"]
tags: ["linux", "hugo"]
categories: []
date: 2020-07-15
---
My Tomsweb site has been on [MoinMoin](http://moinmo.in/) for 11 years and has served me well! However I've just migrated to a staticly generated site builder called [Hugo](https://gohugo.io/). It's stored in [gitlab.com/thomasdstewart/tomsweb/](https://gitlab.com/thomasdstewart/tomsweb/) and the CI/CD generates the pages and serves the content. So now I don't need to run an apache anymore, happy and sad days.
