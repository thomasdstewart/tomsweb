---
title: "IMDB Graph" 
summary: ""
authors: ["thomas"]
tags: ["imdb", "python"]
categories: []
date: 2012-05-10
---
* [imdbgraphdot.py](imdbgraphdot.py)
* [imdbgraph.movies.p](imdbgraph.movies.p)
* [imdbgraph.png](imdbgraph.png)
* [imdbgraph.ps](imdbgraph.ps)
* [imdbgraph.py](imdbgraph.py)
* [imdbgraph.results.p](imdbgraph.results.p)
* [imdbgraph.svg](imdbgraph.svg)
