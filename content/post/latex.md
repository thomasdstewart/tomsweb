---
title: "LaTeX"
summary: ""
authors: ["thomas"]
tags: ["latex"]
categories: []
date: 2013-11-18
aliases: [/tomsweb/LaTeX/]
---
Every so often I find myself recommending LaTeX to people that are either starting a PhD or are struggling with writing their thesis up in Microsoft Word. Over the years I found myself rewriting and improving the same email explaining what LaTeX is and why you should use it. I've decided to just publish it and email people a link instead.

As you probably know [Microsoft Word](http://en.wikipedia.org/wiki/Microsoft_Word) (and [OpenOffice](http://en.wikipedia.org/wiki/OpenOffice)) can be extremely frustrating at times. There is an alternative called LaTeX, it is a different sort of word processor, initially it seems very odd but after you get over the initial change in mindset, you will wonder how you used Word at all to create documents of more than 10 pages. The main advantage is that you just do the writing, then later it makes it pretty for you. This means that while you are writing you don't waste you time with formatting. It does all the formatting for you as well as creating the page of contents, list of figures, page numbers, etc. It is also totally free. I have a few links for you to peruse at your leisure. Don't be intimidated, it a lot easier that it looks. 

A very short page about what it is: http://scottmcpeak.com/latex/whatislatex.html and why to use it: http://latexforhumans.wordpress.com/2008/10/07/why-use-latex/

A short write up about it: http://www.osnews.com/story/10766 and some pros and cons: http://openwetware.org/wiki/Word_vs._LaTeX

Reasons to use it if you're a typesetting geek: http://nitens.org/taraborelli/latex and the detailed Wikipedia page about it: http://en.wikipedia.org/wiki/LaTeX

The main program for LaTeX on Windows is called MiKTEX, you can get it here: http://miktex.org/

Next you need an editor to edit the files, there are loads out there. Notepad will do, my favourite is [Vim](http://www.vim.org/), but there are other ones with useful, helpful and cool features. [My brother](http://hdl.handle.net/10036/3102) recommends: http://www.winshell.de/

Then you will need a thesis template that looks good. My brother recommends: http://www.sunilpatel.co.uk/thesis-template/. The information about this template is actually formatted in the form of a thesis, it gives information about LaTeX in general and information about that template. To see what that template looks like have look at: http://www.sunilpatel.co.uk/wp-content/uploads/2010/08/Thesis-Template-Help.pdf

My favourite guide to learn LaTeX is the "A (Not So) Short Introduction to !LaTeX2e" which is here: http://tobi.oetiker.ch/lshort/lshort.pdf

I did a very quick search for LaTeX and EndNote and there seems to be ways to use EndNote in LaTeX. (http://www.rhizobia.co.nz/latex/convert, http://www.academicproductivity.com/2008/using-endnote-with-latex/). There also seems to be Reference Manager integration too. If you don't like them or don't like !EndNote or want to maintain some sanity then use http://www.mendeley.com/

What about supervisors making corrections? They will still want to continue torturing themselves with Microsoft Word, just use http://latex2rtf.sourceforge.net/ to convert your LaTeX document to RTF (which works in Word). Then supervisors can correct with track changes to their hearts content and you can update your original LaTeX document.

Here is a [real example](https://ore.exeter.ac.uk/repository/bitstream/handle/10036/3102/StewartC.pdf?sequence=1)

Give it at least a try, it could save you hours in the long run. I used it for my final year write up and my brother used it for his PhD thesis. As do many thousands of other people. Let me know how you get on or if you need any help with it. Google is your friend. :-)
