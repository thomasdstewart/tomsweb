---
title: "Machine Room Temperature"
summary: ""
authors: ["thomas"]
tags: ["datacentre", "cooling"]
categories: []
date: 2008-03-16
---
Run away temperature, 10 degrees an hour
[temp.pdf](temp.pdf)

Stable temperature
[temp.ps](temp.ps)
