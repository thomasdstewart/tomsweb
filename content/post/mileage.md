---
title: "Mileage"
summary: ""
authors: ["thomas"]
tags: ["blog", "petrol", "mileage"]
categories: []
date: 2012-05-23 22:32:00
---
TL;DNR - Buy Super Unleaded petrol! (or don't)

I usually buy my petrol from Tesco for my Celica, I mostly use their super unleaded 99 octane. I have tried both the normal and the super, I've not really noticed a massive difference, however the super feels better, but that could just be me imagining it. So I decided to do a not really scientific experiment to find out which one really is best. Is that extra 5p per litre worth it? I get petrol every week and an half or so and fill my 50 litre tank, so thats about [£85](http://www.wolframalpha.com/input/?i=%2850+litres+*+%28%C2%A30.05+%2F+litre%29%29+*+%2852+weeks+%2F+1.5%29) difference.

My Celica does not have mileage readout so I had to calculate it the old fashioned way. Which is: get the tank almost empty, fill it to the brim, take note of the odometer reading, drive until the tank is almost empty, fill it again to the brim and take note of the odometer reader again. That way the petrol sloshing about in the bottom of the tank is not counted. The odometer difference is the total distance and the amount filled the second time is the fuel volume. I did this twice, once with regular and once with super. Between each test I did a complete fill to make sure each test only had the fuel of that test.

On with the results:

| Fuel Type | Miles    | Litres  | Paid    | Cost per Litre | Mileage  | Pounds per mile |
| ---       | ---      | ---     | ---     | ---            | ---      | ---             |
| Regular   | 334.5 mi | 52.09 l | £ 69.23 | £ 1.329        | [29.19 mpg](http://www.wolframalpha.com/input/?i=334.5+Miles+%2F+52.09+Litres) | [4.832 mi / £](http://www.wolframalpha.com/input/?i=%28334.5+Miles+%2F+52.09+Litres%29+%2F+%281.329+%C2%A3%2Flitre%29)
| Super     | 333.3 mi | 50.01 l | £ 68.96 | £ 1.379        | [30.30 mpg](http://www.wolframalpha.com/input/?i=333.3+Miles+%2F+50.01+Litres) | [4.833 mi / £](http://www.wolframalpha.com/input/?i=%28333.3+Miles+%2F+50.01+Litres%29+%2F+%281.379+%C2%A3%2Flitre%29)

So as you can see the numbers are all very similar, the Super is 1 mpg better but as it costs a little more it actually works out the same! So I'm going to stick with super which feels better and gets be an extra 15 miles per tank.
