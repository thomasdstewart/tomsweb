---
title: "Milk to Coffee Ratio"
summary: ""
authors: ["thomas"]
tags: ["blog", "coffee"]
categories: []
date: 2011-06-01 02:09:00
---
Whenever I buy coffee I usually get a small or a medium. Up to now I've always seemed to enjoy the small ones more. I've always assumed it was because a medium was too much of a good thing. Which has lead me to hardly ever get a large.

However I was thinking recently that maybe I was wrong and the small ones just taste better. This lead me to wonder why. Anyone that knows me knows that I love milk. I thought that maybe I liked more milky ones better. I decided to conduct an experiment, over a few days I got a small, medium and large coffee to measure how much milk will fit in each one.

I didn't bother measuring the volume of the paper cups I just read the volume from some text on the bottom of the Starbucks cups, assumed each shot was 30ml and I also did not take into account milk foam or dead space at the top of the cup. Anyway here are the results:

| Cup Size | Cup Volume (ml) | Shots | Coffee Volume (ml) | Total Milk (ml) | Milk to Coffee Ratio |
| ---      | ---             | ---   | ---                | ---             | ---                  |
| Large    | 605.8           | 3     | 3*30=90            | 605.8-90=515.8  | 515.8/90=5.73        |
| Medium   | 473             | 2     | 2*30=60            | 473-60=413      | 413/60=6.88          |
| Small    | 355             | 1     | 1*30=30ml          | 355-30=325      |  325/30=10.83        |

I can now conclusively state that the milk to coffee ratio is highest for small cups, almost double that of the Medium and Large. Where as Large and Medium are relatively similar. Further investigation will be required to measure the cups and shot sizes more accurately. As well as real mugs rather than paper ones. As a side note getting a medium with one shot will result in a milk to coffee ratio of around 15, which could even be too milk for me! However a Large with two shots comes in around at 9 which is similar to the small ratio.

For now on I will just get smalls and if I want more I'll just buy two cups!
