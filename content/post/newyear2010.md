---
title: "New Year's resolutions 2010"
summary: ""
authors: ["thomas"]
tags: ["blog", "resolutions"]
categories: []
date: 2010-01-04 23:35:00
---
It's time for some new years resolutions! Hopefully if they are cast in stone (well blog) there is more of a chance of success.

* Stay super healthy for January:
  * Keeping to my minimum weekly gym schedule, 3 morning swims, 2 morning spins, 2-3 afternoon spins, 2 afternoon yogas, 1-2 weekend weights and cv.
  * No snacks during the day.
  * No cake or chocolate.
  * Herbal tea during the day with no milk.
  * No burgers, chips or beer.
  * No getting drink.
  * Drinking loads of water.
* Read more books, not reference books, what I call story books. I think a realistic goal is one chapter a week. I have started with Terry Pratchett's "Guards! Guards!", feel free to recommend books.
* Start reading LWN again.

