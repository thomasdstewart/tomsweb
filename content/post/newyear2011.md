---
title: "New Year's resolutions 2011"
summary: ""
authors: ["thomas"]
tags: ["blog", "resolutions"]
categories: []
date: 2011-01-04 10:56:00
---
This year seems to have flown by so it's resolutions time again. I've done quite well, I manged to stay relatively healthy and read loads of books but I failed completely to keep up to date with LWN.

Including the books from July I've manged to read 18 books, which is ace! Please make recommendations.

* The Magicians - Lev Grossman
* Woken Furies - Richard K. Morgan
* A Thief of Time - Terry Pratchett
* Making Money - Terry Pratchett
* Ender's Game - Orson Scott Card
* Century Rain - Alastair Reynolds
* Terry Pratchett - Going Postal
* Total Poker - David Spanier
* The Girl with the Dragon Tattoo - Stieg Larsson
* The Girl Who Played with Fire - Stieg Larsson
* The Girl Who Kicked the Hornets' Nest - Stieg Larsson

As for this years ones, It's almost going to be the same.

* Stay super healthy for January and February, except for Little Christmas and Skiing
  * Keeping to my minimum weekly gym schedule, 3 morning swims, 2 morning spins, 2 afternoon spins, 2 afternoon yogas, 1-2 weekend weights and cv.
  * No snacks during the day.
  * No cake or chocolate (except for cake club).
  * Herbal tea during the day with no milk.
  * No burgers, chips or beer.
  * No getting drunk.
  * Drinking loads of water.
* Start reading LWN again.
