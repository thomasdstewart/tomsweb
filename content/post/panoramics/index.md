---
title: "Panoramics"
summary: ""
authors: ["thomas"]
tags: ["photography"]
categories: []
date: 2011-10-19
aliases: [/tomsweb/Panoramics/]
---
This is a panoramic I took from the top of our apartment in Sharm el-Sheikh in Eygpy:
{{< figure src="pansharm2011b.jpg" >}}

This is a panoramic I took from the top of the Panoramic bar in Naama Bay, Sharm el-Sheikh in Eygpy:
{{< figure src="pansharm2011.jpg" >}}

This is a panoramic I took from the top of the Montets lift at the top of one of the glaciers in Val d'Isere (2011):
{{< figure src="pan2011.jpg" >}}

This is a panoramic I took from somewhere in Courchevel or Meribel (2010):
{{< figure src="pan2010.jpg" >}}

This is a panoramic I took at the Oslo ski jump
{{< figure src="oslo.jpg" >}}

These are three panoramics I took when skiing in 2008? (Chamonix and Zermatt)
{{< figure src="pan1.jpg" >}}
{{< figure src="pan2.jpg" >}}
{{< figure src="pan3.jpg" >}}
