---
title: "Passed Intermediate Exam"
summary: ""
authors: ["thomas"]
tags: ["blog", "amaterradio", "hamradio"]
categories: []
date: 2009-11-27 15:22:00
---
I passed my amateur radio Intermediate exam, so now I can kick out 50W! My new call sign is 2E0TDS. My thanks goes to all at SADARS that helped with the practical day and two days of lectures and exams. All I need now is some sort of radio!
