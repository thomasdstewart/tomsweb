---
title: "Petrol Price"
summary: ""
authors: ["thomas"]
tags: ["blog", "petrol"]
categories: []
date: 2011-03-28 21:30:00
---
Petrol Prices are allays on the rise. I decided to do some analysis on some raw data comparing the price of Brend Crude Oil and the price of petrol. The details of the analysis is on my site. I found out that there is a direct correlation between the price of Brent crude oil and the price of petrol at the pump excluding duty and VAT.

I concluded that if the price of Brent crude oil stays at $115 per barrel then the price of petrol will increase to 136.14 +-3.92 pence per litre, it being around 125 at the time of the analysis.
