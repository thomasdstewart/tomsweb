---
title: "New Year's resolutions 2010"
summary: ""
authors: ["thomas"]
tags: ["blog", "resolutions", "reading"]
categories: []
date: 2010-07-05 22:06:00
---
Since Christmas I have been up to loads. I went skiing in March and I went to Cuba in June. I have done well with resolutions, I managed to stay super healthy for January and managed to mostly keep to it since. However I utterly failed to read LWN :-(

I did however keep up the reading and by my standards I have read loads! So far I have read:

* Guards Guards - Terry Pratchett
* Blow Fly - Patricia Cornwell
* Twilight - Stephenie Meyer
* Starship Troopers - Robert A. Heinlein
* Altered Carbon - Richard K. Morgan
* Broken Angels - Richard K. Morgan
* New Moon - Stephenie Meyer

I'm currently reading The Magicians - Lev Grossman. I'm also almost finished the backlog of books everyone suggested after Christmas, so any recommendations we most welcome!
