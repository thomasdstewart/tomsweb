---
title: "RHEL OSP Versions"
summary: ""
authors: ["thomas"]
tags: ["linux", "openstack"]
categories: []
date:  2018-03-22
aliases: [/tomsweb/Stuff/RHELOSPVersions/]
---
Red Hat Enterprise Linux OpenStack Platform Versions:

| Open Stack Code Name | Openstack Release date | RHEL OSP version | RHEL OSP Release  | Delay    |
| ---                  | ---                    | ---              | ---               | ---      |
| Folsom               | 2012-09-27             |  2.1             | 2013-04-10        | 195 days |
| Grizzly              | 2013-04-04             |  3               | 2013-07-10        |  97 days |
| Havana               | 2013-10-17             |  4               | 2013-12-19        |  63 days |
| Icehouse             | 2014-04-17             |  5               | 2014-06-30        |  74 days |
| Juno                 | 2014-10-16             |  6               | 2015-02-09        | 116 days |
| Kilo                 | 2015-04-30             |  7               | 2015-08-05        |  97 days |
| Liberty              | 2015-10-15             |  8               | 2016-04-20        | 188 days |
| Mitaka               | 2016-04-07             |  9               | 2016-08-24        | 139 days |
| Newton               | 2016-10-06             | 10               | 2016-12-15        |  70 days |
| Ocata                | 2017-02-22             | 11               | 2017-05-18        |  85 days |
| Pike                 | 2017-08-30             | 12               | 2017-12-13        | 105 days |
| Queens               | 2018-02-28             |                  |                   |          |
| Rocky                | TBC                    |                  |                   |          |

(111 days delay as average)

 * https://access.redhat.com/articles/1250803
 * https://wiki.openstack.org/wiki/Releases
 * http://releases.openstack.org/
 * https://access.redhat.com/support/policy/updates/openstack/platform/
 * https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux_OpenStack_Platform/2/html/Release_Notes/appe-Documentation-Release_Notes-Revision_History.html
 * https://www.redhat.com/en/about/press-releases/red-hat-powers-cloud-scale-devops-general-availability-red-hat-cloud-suite-and-red-hat-openstack-platform-8
