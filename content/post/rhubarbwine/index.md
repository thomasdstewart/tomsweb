---
title: "Rhubarb Wine"
summary: ""
authors: ["thomas"]
tags: ["wine", "garden"]
categories: []
date: 2021-07-23 11:03:00
---
## Method

 * Pick rhubarb, wash and cut into 1-2cm chunks place in container (20:00 21/07/2021 after cutting 3.348 kg)
 * Add sugar at a 5:3 rhubarb:sugar ratio (21:00 21/07/2021 2.009 kg of sugar was added, mixed and left to stand)
 * Wait for 48-36 hours (13:30 23/07/2021 all sugar dissolved)
 * Filter into demijohn, (2.4L therefore ~0.4L rhubarb juice)
 * Use two teabags in jug, add 300ml of boiling water, brew for 5 min, remove bags and top to 300ml add to demijohn
 * Add 10 grams of raisins
 * Add one heaped teaspoon of yeast into 5L demijohn
 * Add water at a ~1:1.2 rhubarb:water ratio to fill 5L demijohn (2.5L so more like 1:0.75?)
 * take some liquid and read with hydrometer (60)

 racked on 31st Julya, g read 0.988, eg (1.060-0.988)*131 = 9.4% abv

 all but stopped grownth by 3rd Aug
 add 200ml+1 teaspoon of, to demi on 5th
 racked on



## Calculations

A hydrometer measure the density of a liquid, water is 1.0. Lots of dissolved sugar is more dense, so would read higher eg 1.1, the hydrometer floats lower. Once sugar is converted to alcohol is reads lower as water alcohol solution is less dence than water, and thus the hydrometer floats higher

(final gravity 'FG' - original gravity 'OG') * 131.25 = ABV%


{{< figure src="202105.jpg">}}


## References
https://lovelygreens.com/rhubarb-wine-recipe/
https://andhereweare.net/make-rhubarb-wine/


