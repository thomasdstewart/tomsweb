---
title: "Rsync for Windows"
summary: ""
authors: ["thomas"]
tags: ["rsync", "windows"]
categories: []
date: 2010-11-15
aliases: [/tomsweb/Stuff/rsyncforwindows/]
---
[Rsync](http://samba.anu.edu.au/rsync/) is ace. It's also available for Windows as part of the cygwin distro. Every time I need it I have to do a token cygwin install, install rsync, copy out the various rsync files and remote the cygwin install. Here for mostly my pleasure is a very out of date a zip with just the rsync files in!

[cygwin-rsync.zip](cygwin-rsync.zip)
