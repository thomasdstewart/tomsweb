---
title: "Serial Console Cables"
summary: ""
authors: ["thomas"]
tags: ["linux", "hardware", "networking"]
categories: []
date: 2023-12-19 09:53:00
draft: true
---
## Serial Console Cables
It's common for some devices to not have a video output and some sort of keyboard input, be it VGA/HDMI and PS2 or USB. Instead then have a serial console. A [console](https://en.wikipedia.org/wiki/System_console) console being the primary way to interact with the device. This is especially common in servers and network devices.

An advantage to this is that it's a common technology that works out-of-band and thus you can interact with the system without any networking. The disadvantage is there are many different standards at play which make it rather frustrating to test, esp when working with unknown kit.



https://en.wikipedia.org/wiki/D-subminiature
https://en.wikipedia.org/wiki/RS-232
https://en.wikipedia.org/wiki/Serial_port

http://yost.com/computers/RJ45-serial/
https://www.conserver.com/consoles/Cisco/ciscocons.html


Data terminal equipment (DTE) - pc
Data circuit-terminating equipment[1] (DCE) - modem



shows there is demands for azure to aws link, this needs to feed into big rock cloud


