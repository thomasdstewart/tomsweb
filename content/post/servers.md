---
title: "Servers"
summary: ""
authors: ["thomas"]
tags: ["linux", "servers"]
categories: []
date: 2020-07-16
---
## kunzite
Kunzite was my main UML and laterly kvm virtual machine hosted by Bytemark, they are an amazing company, and I highly recommend them! It was long lived, kunzite was first installed on 18th October 2005 with 80Meg ram with 4GB of disk and it finished with 488Meg ram and 20G disk. I've migrated all services to another VM called beryl running on Bytemarks BigV.

## beryl
Beryl is a BigV virtual machine hosted by Bytemark, they are an amazing company, and I highly recommend them!

## onyx
Onyx is a VM run on Mythic Beasts which runs my email and DNS. I would highly recommend them!
