---
title: "Spinning Phrases"
summary: ""
authors: ["thomas"]
tags: ["blog", "gym"]
categories: []
date: 2010-07-22 21:36:00
---
I go to spinning quite a bit and have had I think six teachers over the last year since I started. I think every teacher has one phrase they use most, this not a bad thing, it's just something I have noticed. I present an anonymous list of them here.

* buts over them saddles
* quarter turn
* begin with the end in mind
* go go go go go go go go go go go go go go go go go go go go
* thumbs on the ends of the handle bars
