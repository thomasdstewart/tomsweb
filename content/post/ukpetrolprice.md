---
title: "UK Petrol Prices"
summary: ""
authors: ["thomas"]
tags: ["petrol"]
categories: []
date: 2022-06-28 10:00:00
---
I recently repeated some analysis on the UK Petrol Prices. Slightly different from last time, instead of a large hand written Excel workbook, I wrote a Python script that downloads and processes the data and creates charts from it. This also then plugs into GitLab's CI/CD functionality and also making use of Hugo creates a sort of dynamic updating page of charts. This hosted over on a my gitlab pages: https://thomasdstewart.gitlab.io/ukpetrolprice/
