---
title: "Vim"
summary: ""
authors: ["thomas"]
tags: ["linux", "vim"]
categories: []
date: 2009-05-21
---
[Vim](http://www.vim.org/) is a good text editor my [.vimrc](https://raw.githubusercontent.com/thomasdstewart/dotfiles/master/.vimrc) is available for all to see!

