---
title: "Wedding Meal"
summary: ""
authors: ["thomas"]
tags: ["blog"]
categories: []
date: 2009-08-03 20:16:00
---
I went to Tony and Grainne and Tonys wedding on Friday. It was a lovely day and I wish them all the best in the future.

Anyway, I learnt something while at the hotel that annoyed me. The name of the meal at the wedding reception. Hint: it's not lunch. It's called the [Wedding reception](http://en.wikipedia.org/wiki/Wedding_reception), I'm not entirely sure why, but it really does get on my nerves.
