---
title: "Wikipedia 180"
summary: ""
authors: ["thomas"]
tags: ["wikipedia"]
categories: []
date: 2013-05-07
aliases: [/tomsweb/Stuff/Wikipedia180/]
---
Wikipedia cookies are now good for 180 days instead of 30!
![wikipedia180.png](wikipedia180.png)

Edit April 2013: sadly the are now back to the annoying 30 days:(

Edit April 2020: Horay, we are back again to 365!
![wikipedia365.png](wikipedia365.png)
