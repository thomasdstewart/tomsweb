---
title: "Yoga"
summary: ""
authors: ["thomas"]
tags: ["blog", "gym"]
categories: []
date: 2009-08-12 22:07:00
---
So my [gym](http://www.hertssportsvillage.co.uk/) is closed for 10 days for a refurbishment. They are going to replace all the cardio equipment with awesomely new kit. I'll be able to check my progress online and I'll be able to plug in a video iPod and watch on the screen. The problem is, while it's closed I have no gym to go to! So I decided to try some classes instead. Tonight I tried a Yoga one and it was great, both relaxing and tiring at the same time, I really enjoyed it. I think I'll be going more often in the future and hopefully it will help improve my skiing.
