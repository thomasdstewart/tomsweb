---
title: "Book List"
summary: ""
authors: ["thomas"]
tags: ["book", "other"]
categories: []
date: 2024-10-08
aliases: [/tomsweb/BookList/]
---
Books I've read per year. Title - Author (IDBN) (https://isbnsearch.org/)

2025
 * The Girl and the Moon (The Book of the Ice book 3) - Mark Lawrence (9780008284886)

2024 (3)
 * The Girl and the Mountain (The Book of the Ice book 2) - Mark Lawrence (9781479245529)
 * The Girl and the Stars (The Book of the Ice book 1) - Mark Lawrence (9780008284794)
 * Doctor Sleep - Stephen King (9781451698855)

2023 (10)
 * London Match - Len Deighton (9780802161833)
 * Mexico Set - Len Deighton (9780802161659)
 * Berlin Game - Len Deighton (9780802162151)
 * Assassin's Quest (Farseer trilogy book 3) - Robin Hobb (9780593722848)
 * Royal Assassin (Farseer trilogy book 2) - Robin Hobb (9780593722831)
 * Assassin's Apprentice (Farseer trilogy book 1) - Robin Hobb (9780593722824)
 * Three Men in a Boat - Jerome K. Jerome (9781734852639)
 * 1984 - George Orwell (9781328869333)
 * Jingo - Terry Pratchett (9780062280206)
 * Feet of Clay - Terry Pratchett (9780062275516)

2022 (8)
 * Men At Arms - Terry Pratchett (9780062237408)
 * Guards Guards - Terry Pratchett (9780063373761)
 * My Wonderfull Life: A Memoir - Sid Mynott (9781913306212)
 * The Eye of the World (The Wheel of Time book 1) - Robert Jordan (9781250768681)
 * All These Worlds: Bobiverse Series, Book 3 - Dennis E. Taylor (9781680680607)
 * For We Are Many: Bobiverse Series, Book 2 - Dennis E. Taylor (9781680680591)
 * We Are Legion (We Are Bob): Bobiverse Series, Book 1 - Dennis E. Taylor (9781680680584)
 * Catch-22 - Joseph Heller (9780099496960)

2021 (8)
 * Holy Sister - Mark Lawrence (9781101988930)
 * Grey Sister - Mark Lawrence (9781101988909)
 * Red Sister - Mark Lawrence (9781101988879)
 * The Amber Spyglass: His Dark Materials - Philip Pullman (9780440418566)
 * The Subtle Knife: His Dark Materials 2 - Philip Pullman (9780440418337)
 * Head Over Heel: Seduced by Southern Italy - Chris Harrison (9781857886467)
 * The Republic of Thieves - Scott Lynch (9780553588965)
 * Red Seas Under Red Skies - Scott Lynch (9780553588958)

2020 (4)
 * The Lies of Locke Lamora - Scott Lynch (9780553588941)
 * Novae Spes: A New Hope - Terry Steward (9781528935920)
 * Northern Lights: His Dark Materials 1 - Philip Pullman (9781407106373)
 * Tombland - C.J. Sansom

2019 (6)
 * Lamentation - C.J. Sansom (9780316254977)
 * The Pillars of the Earth - Ken Follett (9780451488336)
 * Heartstone - C.J. Sansom (9780143120650)
 * Murder on the Orient Express - Agatha Christie (9780062073501)
 * Revelation - C.J. Sansom (9780670020515)
 * Dark Fire - C.J. Sansom (9780143036432)

2018 (1)
 * Sovereign - C.J. Sansom (9781101221303)

2017 (3)
 * Armada - Ernest Cline (9786050945911)
 * The Wise Man's Fear - Patrick Rothfuss (9780575081437)
 * The Name of the Wind - Patrick Rothfuss (9780756404741)

2016 (0)

2015 (0)

2014 (6)
 * Jonathan Livingston Seagull - Richard Bach (9781476793313)
 * The Little Prince - Antoine de Saint-Exupéry (9780156012195)
 * Dissolution - C.J. Sansom (9780142004302)
 * The Catcher in the Rye -  J. D. Salinger (9780316769174)
 * Diggers - Terry Pratchett (9780060094935)
 * Stoner - John Williams (9781784877309)
 * Just My Type: A Book About Fonts - Simon Garfield (9781592407460)

2013 (12)
 * Animal Farm - George Orwell (9788187057314)
 * The Road to Wigan Pier - George Orwell (9780156767507)
 * A Small Town in Germany - John le Carré (9780143122609)
 * The Looking Glass War - John le Carré (9780143122593)
 * The Spy Who Came in from the Cold - John le Carré (9780553123104)
 * The Call of Cthulhu - H. P. Lovecraft (9798669574079)
 * The Curious Incident of the Dog in the Night-Time - Mark Haddon (9781400032716)
 * The Russian Concubine - Kate Furnivall (9780425215586)
 * The Boy in the Striped Pyjamas - John Boyne (9780385751537)
 * The Pearl - John Steinbeck (9780140177374)
 * The Shining - Stephen King (9780345806789)
 * Truckers - Terry Pratchett (9780552573337)
 
2012 (9)
 * Monstrous Regiment - Terry Pratchett (9780062307415)
 * Equal Rites - Terry Pratchett (9780062225696)
 * Lolita - Vladimir Nabokov (9780679723165)
 * The Code Book: The Science of Secrecy from Ancient Egypt to Quantum Cryptography - Simon Singh (9780385495325)
 * Fifty Shades of Grey - E. L. James (9781612130293)
 * The Grapes of Wrath - John Steinbeck (9780143039433)
 * Of Mice and Men - John Steinbeck (9780140177398)
 * A Dance with Dragons - George R. R. Martin (9780553385953)
 * A Feast for Crows - George R. R. Martin (9780007582235)

2011 (13)
 * A Storm of Swords - George R. R. Martin (9780553381702)
 * A Clash of Kings - George R. R. Martin (9780553381696)
 * A Game of Thrones - George R. R. Martin (9780553381689)
 * (Harry Potter 1-7 Audio books by Jim Dale) (9781408882290)
 * Women in Love - D. H. Lawrence (9781853260070)
 * Water for Elephants - Sara Gruen (9781565125605)
 * Black Man (Th1rte3n) - Richard K. Morgan (9780345480897)
 * The Professor, the Banker, and the Suicide King: Inside the Richest Poker Game of All Time - Michael Craig (9780446694971)
 * Kitchen Confidential - Anthony Bourdain (9780747553557)
 * Breaking Dawn (The Twilight Saga book 4) - Stephenie Meyer (9780316328326)
 * Eclipse (The Twilight Saga book 3) - Stephenie Meyer (9780316328142)
 * The Cookoo's Egg - Cliff Stoll (9781416507789)
 * The Truth - Terry Pratchett (9780062307361)

2010 (18)
 * The Girl Who Kicked the Hornets' Nest (Millennium book 3) - Stieg Larsson (9780307454560)
 * The Girl Who Played with Fire (Millennium book 2) - Stieg Larsson (9780307454553)
 * The Girl with the Dragon Tattoo (Millennium book 1) - Stieg Larssona (9780307454546)
 * Total Poker - David Spanier (9780671224417)
 * Going Postal - Terry Pratchett (9780062334978)
 * Century Rain - Alastair Reynolds (9780316462662)
 * Ender's Game (The Ender Saga book 1)- Orson Scott Card (9781250773029)
 * Making Money - Terry Pratchett (9780062334992)
 * A Thief of Time - Terry Pratchett (9780062307392)
 * Woken Furies - Richard K. Morgan (9780575081277)
 * The Magicians - Lev Grossman (9780099534440)
 * New Moon (The Twilight Saga book 2)- Stephenie Meyer (9780316327787)
 * Broken Angels - Richard K. Morgan (9780345457714)
 * Altered Carbon - Richard K. Morgan (9780345457684)
 * Starship Troopers - Robert A. Heinlein (9780441014101)
 * Twilight (The Twilight Saga book 1) - Stephenie Meyer (9780316327336)
 * Blow Fly - Patricia Cornwell (9780425266724)
 * Guards Guards - Terry Pratchett (9780063373761)

Earlier
 * The Hobbit - J.R.R. Tolkien (9780547928227)
 * The Fellowship of the Ring (The Lord of the Rings book 1) - J.R.R. Tolkien (9780547928210)
 * The Two Towers (The Lord of the Rings book 2) - J.R.R. Tolkien (9780345339713)
 * The Return of the King (The Lord of the Rings book 3) - J.R.R. Tolkien (9780547952048)

