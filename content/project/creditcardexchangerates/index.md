---
title: "Credit Card Exchange Rates"
summary: ""
authors: ["thomas"]
tags: ["creditcard", "exchangerate", "other"]
categories: []
date: 2013-01-23 
aliases: [/tomsweb/Stuff/CreditCardExchangeRates/]
---
I have a Barclaycard credit card and a Halifax credit card. I recently decided to experiment to try to find out how much it cost to use them abroad to withdraw cash from and ATM. I might be crasy, but I wanted to know! both cards have particular offers that means your don't get charged as much as most cards do to withdraw cash. I did a cash withdrawl of €50 from an ATM on both cards on the same day. Anyway on with the results:

| Card                 | Money Withdrawn | Cost on Bill                             | Overall Rate | Historical rate from ex.com | Historical rate from issuer | Rate difference |
| ---                  | ---             | ---                                      | ---          | ---                         | ---                         | ---             | 
| Barclaycard (Visa)   | €50             | £42.37 (including £1.23 transaction fee) | 1.1801       | 1.2177                      | 1.2225                      | -0.0424         |
| Halifax (Mastercard) | €50             | £40.95                                   | 1.2210       | 1.2177                      | 1.2251                      | -0.0041         |

As I expected my Halifax was better!

See:
* http://www.xe.com/currencytables/
* http://www.visaeurope.com/en/cardholders/exchange_rates.aspx
* https://www.mastercard.com/global/currencyconversion/index.html
