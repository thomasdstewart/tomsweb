---
title: "DDS Destruction"
summary: ""
authors: ["thomas"]
tags: ["sysadmin", "other"]
categories: []
date: 2009-05-21
aliases: [/tomsweb/DDSDestruction/]
---
To see how hardy dds tapes were, I decided to do some tests. I apologise for the image quality. Anyway, on with the show.

## Before

My main test was to see how strong the cassettes actually are, so I decided to run one over with my car. Three times. Surprisingly the only thing to break was the case and the flap at the front, the main body stayed intact. I only rolled over it. I think if I hit it at speed, it would be toast.

Here is a plain old DDS 4 tape, 20G native, 40G compressed:
![ddstape](ddstape.jpg)

## After

![ddstapeafter1](ddstapeafter1.jpg)
![ddstapeafter2](ddstapeafter2.jpg)
![ddstapecomp](ddstapecomp.jpg)

