---
title: "Essential Films"
summary: ""
authors: ["thomas"]
tags: ["movies"]
categories: []
date: 2013-11-24
aliases: [/tomsweb/EssentialFilms/]
---
Here are my essential films. It's somewhere between my favourite films and a list of films that must been seen. It's in alphabetical order and I was originally aiming for the 50 mark however there are too many good films so the current count is 83. The main table is generated from [films.txt](films.txt) with [getimdb.py](getimdb.py) (needs updating from MoinMoin to MD syntax). I've also cross referenced this list with the [Bechdel Test Movie List](http://bechdeltest.com/) on a [Essential Films Bechdel Test]({{< relref "essentialfilmsbechdeltest.md" >}})

|Title|Year|IMDB Rating|Runtime|Genres|Director|Cast|
|---  |--- |---        |---    |---   |---     |--- |
|[2001: A Space Odyssey](http://www.imdb.com/title/tt0062622/)|1968|8.3|149|Adventure, Sci-Fi|Stanley Kubrick|Keir Dullea, Gary Lockwood|
|[A Bridge Too Far](http://www.imdb.com/title/tt0075784/)|1977|7.4|175|Drama, History, War|Richard Attenborough|Siem Vroom, Marlies van Alcmaer|
|[A Fistful of Dollars](http://www.imdb.com/title/tt0058461/)|1964|8.0|99|Action, Drama, Western|Sergio Leone|Clint Eastwood, Marianne Koch|
|[A Good Year](http://www.imdb.com/title/tt0401445/)|2006|7.0|117|Comedy, Drama, Romance|Ridley Scott|Freddie Highmore, Albert Finney|
|[Airplane!](http://www.imdb.com/title/tt0080339/)|1980|7.7|88|Comedy|Jim Abrahams|Kareem Abdul-Jabbar, Lloyd Bridges|
|[Aliens](http://www.imdb.com/title/tt0090605/)|1986|8.3|137|Action, Adventure, Sci-Fi, Thriller|James Cameron|Sigourney Weaver, Carrie Henn|
|[Amélie](http://www.imdb.com/title/tt0211915/)|2001|8.3|122|Comedy, Romance|Jean-Pierre Jeunet|Audrey Tautou, Mathieu Kassovitz|
|[Apollo 13](http://www.imdb.com/title/tt0112384/)|1995|7.6|140|Adventure, Drama, History|Ron Howard|Tom Hanks, Bill Paxton|
|[Back to the Future](http://www.imdb.com/title/tt0088763/)|1985|8.5|116|Adventure, Comedy, Sci-Fi|Robert Zemeckis|Michael J. Fox, Christopher Lloyd|
|[Blade](http://www.imdb.com/title/tt0120611/)|1998|7.1|120|Action, Horror, Sci-Fi|Stephen Norrington|Wesley Snipes, Stephen Dorff|
|[Blade Runner](http://www.imdb.com/title/tt0083658/)|1982|8.1|117|Action, Sci-Fi, Thriller|Ridley Scott|Harrison Ford, Rutger Hauer|
|[Blazing Saddles](http://www.imdb.com/title/tt0071230/)|1974|7.7|93|Comedy, Western|Mel Brooks|Cleavon Little, Gene Wilder|
|[Catch Me If You Can](http://www.imdb.com/title/tt0264464/)|2002|8.1|141|Biography, Crime, Drama|Steven Spielberg|Leonardo DiCaprio, Tom Hanks|
|[Clear and Present Danger](http://www.imdb.com/title/tt0109444/)|1994|6.9|141|Action, Crime, Drama, Thriller|Phillip Noyce|Harrison Ford, Willem Dafoe|
|[Clerks](http://www.imdb.com/title/tt0109445/)|1994|7.7|92|Comedy|Kevin Smith|Brian O'Halloran, Jeff Anderson|
|[Dirty Harry](http://www.imdb.com/title/tt0066999/)|1971|7.7|102|Action, Crime, Thriller|Don Siegel|Clint Eastwood, Harry Guardino|
|[District 9](http://www.imdb.com/title/tt1136608/)|2009|7.9|112|Action, Sci-Fi, Thriller|Neill Blomkamp|Sharlto Copley, Jason Cope|
|[Dr. No](http://www.imdb.com/title/tt0055928/)|1962|7.2|110|Action, Adventure, Thriller|Terence Young|Sean Connery, Ursula Andress|
|[Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb](http://www.imdb.com/title/tt0057012/)|1964|8.4|95|Comedy|Stanley Kubrick|Peter Sellers, George C. Scott|
|[Erin Brockovich](http://www.imdb.com/title/tt0195685/)|2000|7.3|131|Biography, Drama|Steven Soderbergh|Julia Roberts, David Brisbin|
|[Ferris Bueller's Day Off](http://www.imdb.com/title/tt0091042/)|1986|7.8|103|Comedy|John Hughes|Matthew Broderick, Alan Ruck|
|[For a Few Dollars More](http://www.imdb.com/title/tt0059578/)|1965|8.3|132|Western|Sergio Leone|Clint Eastwood, Lee Van Cleef|
|[Forrest Gump](http://www.imdb.com/title/tt0109830/)|1994|8.8|142|Drama, Romance|Robert Zemeckis|Tom Hanks, Rebecca Williams|
|[From Russia with Love](http://www.imdb.com/title/tt0057076/)|1963|7.4|115|Action, Adventure, Thriller|Terence Young|Sean Connery, Daniela Bianchi|
|[Full Metal Jacket](http://www.imdb.com/title/tt0093058/)|1987|8.3|116|Drama, War|Stanley Kubrick|Matthew Modine, Adam Baldwin|
|[Garden State](http://www.imdb.com/title/tt0333766/)|2004|7.4|102|Comedy, Drama, Romance|Zach Braff|Zach Braff, Kenneth Graymez|
|[Goldfinger](http://www.imdb.com/title/tt0058150/)|1964|7.7|110|Action, Adventure, Thriller|Guy Hamilton|Sean Connery, Gert Fröbe|
|[Gone with the Wind](http://www.imdb.com/title/tt0031381/)|1939|8.1|238|Drama, History, Romance, War|Victor Fleming|Thomas Mitchell, Barbara O'Neil|
|[Good Will Hunting](http://www.imdb.com/title/tt0119217/)|1997|8.3|126|Drama, Romance|Gus Van Sant|Matt Damon, Ben Affleck|
|[Ice Cold in Alex](http://www.imdb.com/title/tt0053935/)|1958|7.8|130|Adventure, Drama, War|J. Lee Thompson|John Mills, Sylvia Syms|
|[Inception](http://www.imdb.com/title/tt1375666/)|2010|8.8|148|Action, Adventure, Sci-Fi, Thriller|Christopher Nolan|Leonardo DiCaprio, Joseph Gordon-Levitt|
|[Indiana Jones and the Last Crusade](http://www.imdb.com/title/tt0097576/)|1989|8.2|127|Action, Adventure|Steven Spielberg|Harrison Ford, Sean Connery|
|[Indiana Jones and the Temple of Doom](http://www.imdb.com/title/tt0087469/)|1984|7.6|118|Action, Adventure|Steven Spielberg|Harrison Ford, Kate Capshaw|
|[Jaws](http://www.imdb.com/title/tt0073195/)|1975|8.0|124|Adventure, Thriller|Steven Spielberg|Roy Scheider, Robert Shaw|
|[Jurassic Park](http://www.imdb.com/title/tt0107290/)|1993|8.1|127|Action, Adventure, Sci-Fi, Thriller|Steven Spielberg|Sam Neill, Laura Dern|
|[Just Like Heaven](http://www.imdb.com/title/tt0425123/)|2005|6.7|95|Comedy, Drama, Fantasy, Romance|Mark Waters|Reese Witherspoon, Mark Ruffalo|
|[Lawrence of Arabia](http://www.imdb.com/title/tt0056172/)|1962|8.3|228|Adventure, Biography, Drama, History, War|David Lean|Peter O'Toole, Alec Guinness|
|[Legally Blonde](http://www.imdb.com/title/tt0250494/)|2001|6.3|96|Comedy, Romance|Robert Luketic|Reese Witherspoon, Luke Wilson|
|[Little Miss Sunshine](http://www.imdb.com/title/tt0449059/)|2006|7.8|101|Comedy, Drama|Jonathan Dayton|Abigail Breslin, Greg Kinnear|
|[North by Northwest](http://www.imdb.com/title/tt0053125/)|1959|8.3|136|Adventure, Mystery, Thriller|Alfred Hitchcock|Cary Grant, Eva Marie Saint|
|[Notting Hill](http://www.imdb.com/title/tt0125439/)|1999|7.1|124|Comedy, Drama, Romance|Roger Michell|Julia Roberts, Hugh Grant|
|[Once Upon a Time in the West](http://www.imdb.com/title/tt0064116/)|1968|8.5|165|Western|Sergio Leone|Claudia Cardinale, Henry Fonda|
|[Paint Your Wagon](http://www.imdb.com/title/tt0064782/)|1969|6.6|164|Comedy, Drama, Musical, Romance, Western|Joshua Logan|Lee Marvin, Clint Eastwood|
|[Pan's Labyrinth](http://www.imdb.com/title/tt0457430/)|2006|8.2|118|Drama, Fantasy, War|Guillermo del Toro|Ivana Baquero, Sergi López|
|[Papillon](http://www.imdb.com/title/tt0070511/)|1973|8.0|151|Biography, Crime, Drama|Franklin J. Schaffner|Steve McQueen, Dustin Hoffman|
|[Planes, Trains & Automobiles](http://www.imdb.com/title/tt0093748/)|1987|7.6|93|Comedy, Drama|John Hughes|Steve Martin, John Candy|
|[Play Misty for Me](http://www.imdb.com/title/tt0067588/)|1971|7.0|102|Drama, Thriller|Clint Eastwood|Clint Eastwood, Jessica Walter|
|[Pulp Fiction](http://www.imdb.com/title/tt0110912/)|1994|8.9|154|Crime, Drama|Quentin Tarantino|Tim Roth, Amanda Plummer|
|[Raiders of the Lost Ark](http://www.imdb.com/title/tt0082971/)|1981|8.4|115|Action, Adventure|Steven Spielberg|Harrison Ford, Karen Allen|
|[Requiem for a Dream](http://www.imdb.com/title/tt0180093/)|2000|8.3|102|Drama|Darren Aronofsky|Ellen Burstyn, Jared Leto|
|[Ringu](http://www.imdb.com/title/tt0178868/)|1998|7.2|96|Horror, Mystery|Hideo Nakata|Nanako Matsushima, Miki Nakatani|
|[Roman Holiday](http://www.imdb.com/title/tt0046250/)|1953|8.0|118|Comedy, Romance|William Wyler|Gregory Peck, Audrey Hepburn|
|[Romeo + Juliet](http://www.imdb.com/title/tt0117509/)|1996|6.7|120|Drama, Romance|Baz Luhrmann|Leonardo DiCaprio, Claire Danes|
|[Saving Private Ryan](http://www.imdb.com/title/tt0120815/)|1998|8.6|169|Drama, War|Steven Spielberg|Tom Hanks, Tom Sizemore|
|[Scarface](http://www.imdb.com/title/tt0086250/)|1983|8.3|170|Crime, Drama|Brian De Palma|Al Pacino, Steven Bauer|
|[Schindler's List](http://www.imdb.com/title/tt0108052/)|1993|8.9|195|Biography, Drama, History|Steven Spielberg|Liam Neeson, Ben Kingsley|
|[Snatch](http://www.imdb.com/title/tt0208092/)|2000|8.3|104|Comedy, Crime|Guy Ritchie|Benicio Del Toro, Dennis Farina|
|[Spirited Away](http://www.imdb.com/title/tt0245429/)|2001|8.6|125|Animation, Adventure, Family, Fantasy, Mystery|Hayao Miyazaki|Rumi Hiiragi, Miyu Irino|
|[Taken](http://www.imdb.com/title/tt0936501/)|2008|7.8|90|Action, Thriller|Pierre Morel|Liam Neeson, Maggie Grace|
|[Terminator 2: Judgment Day](http://www.imdb.com/title/tt0103064/)|1991|8.5|137|Action, Sci-Fi|James Cameron|Arnold Schwarzenegger, Linda Hamilton|
|[The Big Lebowski](http://www.imdb.com/title/tt0118715/)|1998|8.1|117|Comedy, Crime, Sport|Joel Coen|Jeff Bridges, John Goodman|
|[The Blues Brothers](http://www.imdb.com/title/tt0080455/)|1980|7.9|133|Adventure, Comedy, Crime, Music, Musical|John Landis|Tom Erhart, Gerald Walling|
|[The Breakfast Club](http://www.imdb.com/title/tt0088847/)|1985|7.9|97|Comedy, Drama|John Hughes|Emilio Estevez, Paul Gleason|
|[The Conversation](http://www.imdb.com/title/tt0071360/)|1974|7.8|113|Drama, Mystery, Thriller|Francis Ford Coppola|Gene Hackman, John Cazale|
|[The Departed](http://www.imdb.com/title/tt0407887/)|2006|8.5|151|Crime, Drama, Thriller|Martin Scorsese|Leonardo DiCaprio, Matt Damon|
|[The Fugitive](http://www.imdb.com/title/tt0106977/)|1993|7.8|130|Action, Crime, Drama, Mystery, Thriller|Andrew Davis|Harrison Ford, Tommy Lee Jones|
|[The Godfather](http://www.imdb.com/title/tt0068646/)|1972|9.2|175|Crime, Drama|Francis Ford Coppola|Marlon Brando, Al Pacino|
|[The Good, the Bad and the Ugly](http://www.imdb.com/title/tt0060196/)|1966|8.8|161|Western|Sergio Leone|Eli Wallach, Clint Eastwood|
|[The Graduate](http://www.imdb.com/title/tt0061722/)|1967|8.0|106|Comedy, Drama, Romance|Mike Nichols|Anne Bancroft, Dustin Hoffman|
|[The Great Escape](http://www.imdb.com/title/tt0057115/)|1963|8.2|172|Adventure, Drama, History, Thriller, War|John Sturges|Steve McQueen, James Garner|
|[The Hangover](http://www.imdb.com/title/tt1119646/)|2009|7.7|100|Comedy|Todd Phillips|Bradley Cooper, Ed Helms|
|[The Little Mermaid](http://www.imdb.com/title/tt0097757/)|1989|7.6|83|Animation, Family, Fantasy, Musical, Romance|Ron Clements|Rene Auberjonois, Christopher Daniel Barnes|
|[The Matrix](http://www.imdb.com/title/tt0133093/)|1999|8.7|136|Action, Sci-Fi|Lana Wachowski|Keanu Reeves, Laurence Fishburne|
|[The Pianist](http://www.imdb.com/title/tt0253474/)|2002|8.5|150|Biography, Drama, Music, War|Roman Polanski|Adrien Brody, Emilia Fox|
|[The Shawshank Redemption](http://www.imdb.com/title/tt0111161/)|1994|9.3|142|Drama|Frank Darabont|Tim Robbins, Morgan Freeman|
|[The Shining](http://www.imdb.com/title/tt0081505/)|1980|8.4|146|Drama, Horror|Stanley Kubrick|Jack Nicholson, Shelley Duvall|
|[The Silence of the Lambs](http://www.imdb.com/title/tt0102926/)|1991|8.6|118|Crime, Drama, Thriller|Jonathan Demme|Jodie Foster, Lawrence A. Bonney|
|[The Wicker Man](http://www.imdb.com/title/tt0070917/)|1973|7.5|88|Horror, Mystery, Thriller|Robin Hardy|Edward Woodward, Christopher Lee|
|[Three Colors: Blue](http://www.imdb.com/title/tt0108394/)|1993|7.9|94|Drama, Music, Mystery, Romance|Krzysztof Kieslowski|Juliette Binoche, Benoît Régent|
|[Trainspotting](http://www.imdb.com/title/tt0117951/)|1996|8.1|93|Drama|Danny Boyle|Ewan McGregor, Ewen Bremner|
|[Uncle Buck](http://www.imdb.com/title/tt0098554/)|1989|7.0|100|Comedy|John Hughes|John Candy, Jean Louisa Kelly|
|[Walk the Line](http://www.imdb.com/title/tt0358273/)|2005|7.8|136|Biography, Drama, Music, Romance|James Mangold|Joaquin Phoenix, Reese Witherspoon|
|[Where Eagles Dare](http://www.imdb.com/title/tt0065207/)|1968|7.7|158|Action, Adventure, War|Brian G. Hutton|Richard Burton, Clint Eastwood|


|Decade|Count|
|---   |---  |
|1990|21|
|2000|18|
|1980|16|
|1960|14|
|1970|9|
|1950|3|
|2010|1|
|1930|1|


|Genre|Count|
|---  |---  |
|Drama|42|
|Comedy|23|
|Adventure|20|
|Action|20|
|Thriller|20|
|Romance|16|
|Crime|13|
|Sci-Fi|10|
|War|10|
|Mystery|7|
|Biography|7|
|Western|6|
|History|6|
|Music|4|
|Fantasy|4|
|Horror|4|
|Musical|3|
|Family|2|
|Animation|2|
|Sport|1|


|Director|Count|
|---     |---  |
|Steven Spielberg|8|
|Stanley Kubrick|4|
|Sergio Leone|4|
|John Hughes|4|
|Terence Young|2|
|Ridley Scott|2|
|James Cameron|2|
|Francis Ford Coppola|2|
|Robert Zemeckis|2|


|Actor|Count|
|---  |---  |
|Harrison Ford|7|
|Clint Eastwood|7|
|Arnold Montey|6|
|Jimmy Star|5|
|Sean Connery|5|
|Antonio Molino Rojo|4|
|Richard Farnsworth|4|
|Frank Braña|4|
|Tom Hanks|4|
|Benito Stefanelli|4|
|Aldo Sambrell|4|
|Leonardo DiCaprio|4|
|Lorenzo Robledo|4|
|Enrique Santiago|3|
|Bob Simmons|3|
|Paul Maxwell|3|
|Richard Attenborough|3|
|Bob Harks|3|
|Nikki Van der Zyl|3|
|John Candy|3|
|Victor Harrington|3|
|Burnell Tucker|3|
|George Fargo|3|
|Lois Maxwell|3|
|Philip Stone|3|
|Laurence Herder|3|
|Ted Grossman|3|
|Xander Berkeley|3|
|Peter Evans|3|
|Steven Spielberg|3|
|Roy Beck|3|
|Harry Fielder|3|
|Joseph Bradley|3|
|Boyd Cabeen|3|
|Mario Brega|3|
|Bernard Lee|3|
|Reese Witherspoon|3|
|Matt Damon|3|
|Pat Roach|3|
|Arthur Tovey|3|
|Ricardo Palacios|3|
|Vivian Kubrick|3|
|David Daniel|3|
|Denholm Elliott|3|
|Pauline Chamberlain|3|
|Aileen Lewis|3|
|Mark Rolston|3|
|Kadrolsha Ona Carole|3|
|Anthony Chinn|3|
|Edie McClurg|3|
|Antonio Palombi|3|
|Nazzareno Natale|3|


