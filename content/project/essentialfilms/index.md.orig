---
title: "Essential Films"
summary: ""
authors: ["thomas"]
tags: ["movies"]
categories: []
date: 2013-11-24
---
Here are my essential films. It's somewhere between my favourite films and a list of films that must been seen. It's in alphabetical order and I was originally aiming for the 50 mark however there are too many good films so the current count is 82. The main table is generated from [films.txt](films.txt) with http://www.stewarts.org.uk/stuff/getimdb.py (needs updating from MoinMoin to MD syntax). I've also cross referenced this list with the [Bechdel Test Movie List](http://bechdeltest.com/) on a [Essential Films Bechdel Test]({{< relref "essentialfilmsbechdeltest.md" >}})

|Title|Year|IMDB Rating|Runtime|Genres|Director|Cast|
|---  |--- |---        |---    |---   |---     |--- |
|[2001: A Space Odyssey](http://www.imdb.com/title/tt0062622/)|1968|8.3|160|Adventure, Mystery, Sci-Fi|Stanley Kubrick|Keir Dullea, Gary Lockwood|
|[A Fistful of Dollars](http://www.imdb.com/title/tt0058461/)|1964|8.0|99|Western|Sergio Leone|Clint Eastwood, Marianne Koch|
|[A Good Year](http://www.imdb.com/title/tt0401445/)|2006|6.8|117|Comedy, Drama, Romance|Ridley Scott|Freddie Highmore, Albert Finney|
|[Airplane!](http://www.imdb.com/title/tt0080339/)|1980|7.8|88|Comedy|Jim Abrahams|Kareem Abdul-Jabbar, Lloyd Bridges|
|[Aliens](http://www.imdb.com/title/tt0090605/)|1986|8.5|137|Action, Adventure, Sci-Fi, Thriller|James Cameron|Sigourney Weaver, Carrie Henn|
|[Amélie](http://www.imdb.com/title/tt0211915/)|2001|8.4|122|Comedy, Romance|Jean-Pierre Jeunet|Audrey Tautou, Mathieu Kassovitz|
|[Apollo 13](http://www.imdb.com/title/tt0112384/)|1995|7.5|140|Adventure, Drama, History|Ron Howard|Tom Hanks, Bill Paxton|
|[Back to the Future](http://www.imdb.com/title/tt0088763/)|1985|8.5|116|Adventure, Comedy, Sci-Fi|Robert Zemeckis|Michael J. Fox, Christopher Lloyd|
|[Blade](http://www.imdb.com/title/tt0120611/)|1998|7.0|120|Action, Fantasy, Horror|Stephen Norrington|Wesley Snipes, Kris Kristofferson|
|[Blade Runner](http://www.imdb.com/title/tt0083658/)|1982|8.2|117|Drama, Sci-Fi, Thriller|Ridley Scott|Harrison Ford, Rutger Hauer|
|[Blazing Saddles](http://www.imdb.com/title/tt0071230/)|1974|7.7|93|Comedy, Western|Mel Brooks|Cleavon Little, Gene Wilder|
|[Catch Me If You Can](http://www.imdb.com/title/tt0264464/)|2002|7.9|141|Biography, Crime, Drama|Steven Spielberg|Leonardo !DiCaprio, Tom Hanks|
|[Clear and Present Danger](http://www.imdb.com/title/tt0109444/)|1994|6.8|141|Action, Drama, Thriller|Phillip Noyce|Harrison Ford, Willem Dafoe|
|[Clerks.](http://www.imdb.com/title/tt0109445/)|1994|7.8|92|Comedy|Kevin Smith|Brian O'Halloran, Jeff Anderson|
|[Dirty Harry](http://www.imdb.com/title/tt0066999/)|1971|7.8|102|Action, Crime, Thriller|Don Siegel|Clint Eastwood, Harry Guardino|
|[District 9](http://www.imdb.com/title/tt1136608/)|2009|8.0|112|Action, Sci-Fi, Thriller|Neill Blomkamp|Sharlto Copley, Jason Cope|
|[Dr. No](http://www.imdb.com/title/tt0055928/)|1962|7.3|110|Action, Adventure, Crime, Thriller|Terence Young|Sean Connery, Ursula Andress|
|[Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb](http://www.imdb.com/title/tt0057012/)|1964|8.5|95|Comedy, Sci-Fi, War|Stanley Kubrick|Peter Sellers, George C. Scott|
|[Erin Brockovich](http://www.imdb.com/title/tt0195685/)|2000|7.2|131|Biography, Drama|Steven Soderbergh|Julia Roberts, David Brisbin|
|[Ferris Bueller's Day Off](http://www.imdb.com/title/tt0091042/)|1986|7.8|103|Comedy, Drama|John Hughes|Matthew Broderick, Alan Ruck|
|[For a Few Dollars More](http://www.imdb.com/title/tt0059578/)|1965|8.3|132|Western|Sergio Leone|Clint Eastwood, Lee Van Cleef|
|[Forrest Gump](http://www.imdb.com/title/tt0109830/)|1994|8.7|142|Drama, Romance|Robert Zemeckis|Tom Hanks, Rebecca Williams|
|[From Russia with Love](http://www.imdb.com/title/tt0057076/)|1963|7.5|115|Action, Adventure, Crime, Thriller|Terence Young|Sean Connery, Daniela Bianchi|
|[Full Metal Jacket](http://www.imdb.com/title/tt0093058/)|1987|8.3|116|Drama, War|Stanley Kubrick|Matthew Modine, Adam Baldwin|
|[Garden State](http://www.imdb.com/title/tt0333766/)|2004|7.6|102|Comedy, Drama, Romance|Zach Braff|Zach Braff, Kenneth Graymez|
|[Goldfinger](http://www.imdb.com/title/tt0058150/)|1964|7.8|110|Action, Adventure, Crime, Thriller|Guy Hamilton|Sean Connery, Honor Blackman|
|[Gone with the Wind](http://www.imdb.com/title/tt0031381/)|1939|8.2|238|Drama, Romance, War|Victor Fleming|Thomas Mitchell, Barbara O'Neil|
|[Good Will Hunting](http://www.imdb.com/title/tt0119217/)|1997|8.2|126|Drama|Gus Van Sant|Matt Damon, Robin Williams|
|[Ice Cold in Alex](http://www.imdb.com/title/tt0053935/)|1958|7.8|129|Drama, War|J. Lee Thompson|John Mills, Sylvia Syms|
|[Inception](http://www.imdb.com/title/tt1375666/)|2010|8.8|148|Action, Adventure, Mystery, Sci-Fi, Thriller|Christopher Nolan|Leonardo !DiCaprio, Joseph Gordon-Levitt|
|[Indiana Jones and the Last Crusade](http://www.imdb.com/title/tt0097576/)|1989|8.3|127|Action, Adventure|Steven Spielberg|Harrison Ford, Sean Connery|
|[Indiana Jones and the Temple of Doom](http://www.imdb.com/title/tt0087469/)|1984|7.6|118|Action, Adventure|Steven Spielberg|Harrison Ford, Kate Capshaw|
|[Jaws](http://www.imdb.com/title/tt0073195/)|1975|8.1|124|Adventure, Horror, Thriller|Steven Spielberg|Roy Scheider, Robert Shaw|
|[Jurassic Park](http://www.imdb.com/title/tt0107290/)|1993|8.0|127|Adventure, Sci-Fi|Steven Spielberg|Sam Neill, Laura Dern|
|[Just Like Heaven](http://www.imdb.com/title/tt0425123/)|2005|6.6|95|Comedy, Fantasy, Romance|Mark Waters|Reese Witherspoon, Mark Ruffalo|
|[Lawrence of Arabia](http://www.imdb.com/title/tt0056172/)|1962|8.4|216|Adventure, Biography, Drama, History, War|David Lean|Peter O'Toole, Alec Guinness|
|[Legally Blonde](http://www.imdb.com/title/tt0250494/)|2001|6.1|96|Comedy, Romance|Robert Luketic|Reese Witherspoon, Luke Wilson|
|[Little Miss Sunshine](http://www.imdb.com/title/tt0449059/)|2006|7.9|101|Adventure, Comedy, Drama|Jonathan Dayton|Abigail Breslin, Greg Kinnear|
|[North by Northwest](http://www.imdb.com/title/tt0053125/)|1959|8.5|136|Action, Adventure, Crime, Mystery, Thriller|Alfred Hitchcock|Cary Grant, Eva Marie Saint|
|[Notting Hill](http://www.imdb.com/title/tt0125439/)|1999|6.9|124|Comedy, Drama, Romance|Roger Michell|Julia Roberts, Hugh Grant|
|[Once Upon a Time in the West](http://www.imdb.com/title/tt0064116/)|1968|8.7|Italy:175|Adventure, Western|Sergio Leone|Claudia Cardinale, Henry Fonda|
|[Paint Your Wagon](http://www.imdb.com/title/tt0064782/)|1969|6.5|158|Comedy, Drama, Musical, Romance, Western|Joshua Logan|Lee Marvin, Clint Eastwood|
|[Pan's Labyrinth](http://www.imdb.com/title/tt0457430/)|2006|8.3|118|Drama, Fantasy, War|Guillermo del Toro|Ivana Baquero, Sergi López|
|[Papillon](http://www.imdb.com/title/tt0070511/)|1973|8.0|151|Biography, Crime, Drama|Franklin J. Schaffner|Steve !McQueen, Dustin Hoffman|
|[Planes, Trains & Automobiles](http://www.imdb.com/title/tt0093748/)|1987|7.5|93|Comedy, Drama|John Hughes|Steve Martin, John Candy|
|[Play Misty for Me](http://www.imdb.com/title/tt0067588/)|1971|7.0|102|Crime, Drama, Romance, Thriller|Clint Eastwood|Clint Eastwood, Jessica Walter|
|[Pulp Fiction](http://www.imdb.com/title/tt0110912/)|1994|9.0|154|Crime, Drama, Thriller|Quentin Tarantino|Tim Roth, Amanda Plummer|
|[Raiders of the Lost Ark](http://www.imdb.com/title/tt0082971/)|1981|8.6|115|Action, Adventure|Steven Spielberg|Harrison Ford, Karen Allen|
|[Requiem for a Dream](http://www.imdb.com/title/tt0180093/)|2000|8.4|102|Drama|Darren Aronofsky|Ellen Burstyn, Jared Leto|
|[Ringu](http://www.imdb.com/title/tt0178868/)|1998|7.3|96|Horror, Mystery, Thriller|Hideo Nakata|Nanako Matsushima, Miki Nakatani|
|[Roman Holiday](http://www.imdb.com/title/tt0046250/)|1953|8.1|118|Comedy, Drama, Romance|William Wyler|Gregory Peck, Audrey Hepburn|
|[Romeo + Juliet](http://www.imdb.com/title/tt0117509/)|1996|6.8|120|Drama, Romance|Baz Luhrmann|Leonardo !DiCaprio, Claire Danes|
|[Saving Private Ryan](http://www.imdb.com/title/tt0120815/)|1998|8.6|169|Action, Drama, War|Steven Spielberg|Tom Hanks, Tom Sizemore|
|[Scarface](http://www.imdb.com/title/tt0086250/)|1983|8.3|170|Crime, Drama|Brian De Palma|Al Pacino, Steven Bauer|
|[Schindler's List](http://www.imdb.com/title/tt0108052/)|1993|8.9|195|Biography, Drama, History, War|Steven Spielberg|Liam Neeson, Ben Kingsley|
|[Snatch.](http://www.imdb.com/title/tt0208092/)|2000|8.3|104|Crime, Thriller|Guy Ritchie|Benicio Del Toro, Dennis Farina|
|[Spirited Away](http://www.imdb.com/title/tt0245429/)|2001|8.6|125|Animation, Adventure, Family, Fantasy|Hayao Miyazaki|Daveigh Chase, Suzanne Pleshette|
|[Taken](http://www.imdb.com/title/tt0936501/)|2008|7.8|93|Action, Crime, Thriller|Pierre Morel|Liam Neeson, Maggie Grace|
|[Terminator 2: Judgment Day](http://www.imdb.com/title/tt0103064/)|1991|8.5|137|Action, Sci-Fi, Thriller|James Cameron|Arnold Schwarzenegger, Linda Hamilton|
|[The Big Lebowski](http://www.imdb.com/title/tt0118715/)|1998|8.2|117|Comedy, Crime|Joel Coen|Jeff Bridges, John Goodman|
|[The Blues Brothers](http://www.imdb.com/title/tt0080455/)|1980|7.9|133|Action, Comedy, Crime, Music|John Landis|John Belushi, Dan Aykroyd|
|[The Breakfast Club](http://www.imdb.com/title/tt0088847/)|1985|7.9|97|Comedy, Drama|John Hughes|Emilio Estevez, Paul Gleason|
|[The Conversation](http://www.imdb.com/title/tt0071360/)|1974|7.9|113|Drama, Mystery, Thriller|Francis Ford Coppola|Gene Hackman, John Cazale|
|[The Departed](http://www.imdb.com/title/tt0407887/)|2006|8.5|151|Crime, Drama, Thriller|Martin Scorsese|Leonardo !DiCaprio, Matt Damon|
|[The Fugitive](http://www.imdb.com/title/tt0106977/)|1993|7.8|130|Action, Adventure, Crime, Mystery, Thriller|Andrew Davis|Harrison Ford, Tommy Lee Jones|
|[The Godfather](http://www.imdb.com/title/tt0068646/)|1972|9.2|175|Crime, Drama|Francis Ford Coppola|Marlon Brando, Al Pacino|
|[The Good, the Bad and the Ugly](http://www.imdb.com/title/tt0060196/)|1966|9.0|161|Adventure, Western|Sergio Leone|Eli Wallach, Clint Eastwood|
|[The Graduate](http://www.imdb.com/title/tt0061722/)|1967|8.1|106|Comedy, Drama, Romance|Mike Nichols|Anne Bancroft, Dustin Hoffman|
|[The Great Escape](http://www.imdb.com/title/tt0057115/)|1963|8.3|172|Adventure, Drama, History, Thriller, War|John Sturges|Steve !McQueen, James Garner|
|[The Hangover](http://www.imdb.com/title/tt1119646/)|2009|7.8|100|Comedy|Todd Phillips|Bradley Cooper, Ed Helms|
|[The Little Mermaid](http://www.imdb.com/title/tt0097757/)|1989|7.5|83|Animation, Family, Fantasy, Musical, Romance|Ron Clements|Rene Auberjonois, Christopher Daniel Barnes|
|[The Matrix](http://www.imdb.com/title/tt0133093/)|1999|8.7|136|Action, Adventure, Sci-Fi|Andy Wachowski|Keanu Reeves, Laurence Fishburne|
|[The Pianist](http://www.imdb.com/title/tt0253474/)|2002|8.5|150|Biography, Drama, History, War|Roman Polanski|Adrien Brody, Emilia Fox|
|[The Shawshank Redemption](http://www.imdb.com/title/tt0111161/)|1994|9.3|142|Crime, Drama|Frank Darabont|Tim Robbins, Morgan Freeman|
|[The Shining](http://www.imdb.com/title/tt0081505/)|1980|8.5|142::(cut)|Horror, Mystery|Stanley Kubrick|Jack Nicholson, Shelley Duvall|
|[The Silence of the Lambs](http://www.imdb.com/title/tt0102926/)|1991|8.7|118|Crime, Drama, Thriller|Jonathan Demme|Jodie Foster, Lawrence A. Bonney|
|[The Wicker Man](http://www.imdb.com/title/tt0070917/)|1973|7.6|88|Horror, Mystery, Thriller|Robin Hardy|Edward Woodward, Christopher Lee|
|[Three Colors: Blue](http://www.imdb.com/title/tt0108394/)|1993|7.9|98|Drama, Music, Mystery, Romance|Krzysztof Kieslowski|Juliette Binoche, Benoît Régent|
|[Trainspotting](http://www.imdb.com/title/tt0117951/)|1996|8.2|94|Crime, Drama|Danny Boyle|Ewan !McGregor, Ewen Bremner|
|[Uncle Buck](http://www.imdb.com/title/tt0098554/)|1989|6.8|100|Comedy, Drama, Family|John Hughes|John Candy, Jean Louisa Kelly|
|[Walk the Line](http://www.imdb.com/title/tt0358273/)|2005|7.8|136|Biography, Drama, Music, Romance|James Mangold|Joaquin Phoenix, Reese Witherspoon|
|[Where Eagles Dare](http://www.imdb.com/title/tt0065207/)|1968|7.6|158|Action, Adventure, War|Brian G. Hutton|Richard Burton, Clint Eastwood|

|Decade|Count|
|---   |---  |
|1990|21|
|2000|18|
|1980|16|
|1960|14|
|1970|8|
|1950|3|
|2010|1|
|1930|1|

|Genre|Count|
|---  |---  |
|Drama|41|
|Thriller|23|
|Adventure|23|
|Comedy|22|
|Crime|20|
|Action|20|
|Romance|16|
|War|11|
|Sci-Fi|10|
|Mystery|9|
|Biography|7|
|Western|6|
|Horror|5|
|History|5|
|Fantasy|5|
|Music|3|
|Family|3|
|Musical|2|
|Animation|2|

|Director|Count|
|---     |---  |
|Steven Spielberg|8|
|Sergio Leone|4|
|Stanley Kubrick|4|
|John Hughes|4|
|Robert Zemeckis|2|
|Terence Young|2|
|Ridley Scott|2|
|Francis Ford Coppola|2|
|James Cameron|2|

|Actor|Count|
|---  |---  |
|Harrison Ford|7|
|Clint Eastwood|7|
|Benito Stefanelli|4|
|Aldo Sambrell|4|
|Antonio Molino Rojo|4|
|Lorenzo Robledo|4|
|Tom Hanks|4|
|Richard Farnsworth|4|
|Leonardo DiCaprio|4|
|Sean Connery|4|
|Frank Braña|4|
|Nikki Van der Zyl|3|
|Reese Witherspoon|3|
|Philip Stone|3|
|Steven Spielberg|3|
|Bob Simmons|3|
|Enrique Santiago|3|
|Mark Rolston|3|
|Pat Roach|3|
|Ricardo Palacios|3|
|Nazzareno Natale|3|
|Edie McClurg|3|
|Lois Maxwell|3|
|Bernard Lee|3|
|Vivian Kubrick|3|
|Ted Grossman|3|
|George Fargo|3|
|Matt Damon|3|
|Anthony Chinn|3|
|John Candy|3|
|Mario Brega|3|
|Xander Berkeley|3|
